<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 06-07-2014
 * Time: 13:00
 */

namespace Andre\ScrambleBundle\Controller;


use Andre\Scramble\WordRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Andre\ScrambleBundle\Entity\Word;

class PlayController extends Controller {



    public function showWordAction()
    {

        $answers = array();
        $correctAnswers = array();
        $points = 0;


        $repository = $this->get('scramble.repository.word');


        $numberOfWords = $repository->findNumberOfWords();
        $randomNumber = rand(1, $numberOfWords);

        $randomWord = $repository->findRandomWord($randomNumber);

        foreach($randomWord as $word)
        {
            $shufledWord = str_shuffle($word->getWord());
            $shufledWord = strtoupper ( $shufledWord );
        }

        $possibleAnswers = $this->possibleAnswers($repository, $shufledWord);

       return $this->render('AndreScrambleBundle::play.html.twig',array('shufledWord'=>$shufledWord,
           'possibleAnswers'=>$possibleAnswers));
    }


    public function possibleAnswers(WordRepositoryInterface $repository, $shufledWord)
    {

        //*** CASE ***//
        //$shufledWord = "ANA";
        //*** CASE ***//
        $shufledWordLetterCounter = $this->letterCounter($shufledWord);

        $allWords = $repository->findAllWords();




        foreach($allWords as $allWord)
        {
            $possibleAnswer =$allWord->getWord();
            $possibleAnswer = strtoupper ( $possibleAnswer );


            $numberOfLetterOfPossibleAnswer = strlen($possibleAnswer);
            if($numberOfLetterOfPossibleAnswer >1)
            {
                $possibleAnswerLetterCounter = $this->letterCounter($possibleAnswer);
            }
            else{continue;}

            if(($possibleAnswerLetterCounter["a"]<=$shufledWordLetterCounter["a"])&&
                ($possibleAnswerLetterCounter["b"]<=$shufledWordLetterCounter["b"])&&
                ($possibleAnswerLetterCounter["c"]<=$shufledWordLetterCounter["c"])&&
                ($possibleAnswerLetterCounter["d"]<=$shufledWordLetterCounter["d"])&&
                ($possibleAnswerLetterCounter["e"]<=$shufledWordLetterCounter["e"])&&
                ($possibleAnswerLetterCounter["f"]<=$shufledWordLetterCounter["f"])&&
                ($possibleAnswerLetterCounter["g"]<=$shufledWordLetterCounter["g"])&&
                ($possibleAnswerLetterCounter["h"]<=$shufledWordLetterCounter["h"])&&
                ($possibleAnswerLetterCounter["i"]<=$shufledWordLetterCounter["i"])&&
                ($possibleAnswerLetterCounter["j"]<=$shufledWordLetterCounter["j"])&&
                ($possibleAnswerLetterCounter["k"]<=$shufledWordLetterCounter["k"])&&
                ($possibleAnswerLetterCounter["l"]<=$shufledWordLetterCounter["l"])&&
                ($possibleAnswerLetterCounter["m"]<=$shufledWordLetterCounter["m"])&&
                ($possibleAnswerLetterCounter["n"]<=$shufledWordLetterCounter["n"])&&
                ($possibleAnswerLetterCounter["o"]<=$shufledWordLetterCounter["o"])&&
                ($possibleAnswerLetterCounter["p"]<=$shufledWordLetterCounter["p"])&&
                ($possibleAnswerLetterCounter["q"]<=$shufledWordLetterCounter["q"])&&
                ($possibleAnswerLetterCounter["r"]<=$shufledWordLetterCounter["r"])&&
                ($possibleAnswerLetterCounter["s"]<=$shufledWordLetterCounter["s"])&&
                ($possibleAnswerLetterCounter["t"]<=$shufledWordLetterCounter["t"])&&
                ($possibleAnswerLetterCounter["u"]<=$shufledWordLetterCounter["u"])&&
                ($possibleAnswerLetterCounter["v"]<=$shufledWordLetterCounter["v"])&&
                ($possibleAnswerLetterCounter["w"]<=$shufledWordLetterCounter["w"])&&
                ($possibleAnswerLetterCounter["x"]<=$shufledWordLetterCounter["x"])&&
                ($possibleAnswerLetterCounter["y"]<=$shufledWordLetterCounter["y"])&&
                ($possibleAnswerLetterCounter["z"]<=$shufledWordLetterCounter["z"])
            )
            {
                $answersAvailable[]=$possibleAnswer;
            }
        }

        return $answersAvailable;
    }



    public function letterCounter($possibleAnswer)
    {

        $count = 0;

        $a=$b=$c=$d=$e=$f=$g=$h=$i=$j=$k=$l=$m=$n=$o=$p=$q=$r=$s=$t=$u=$v=$w=$x=$y=$z=0;

        $letterCounter=["a"=>$a,"b"=>$b,"c"=>$c,"d"=>$d,"e"=>$e,"f"=>$f,"g"=>$g,"h"=>$h,"i"=>$i,"j"=>$j,"k"=>$k,
            "l"=>$l,"m"=>$m,"n"=>$n,"o"=>$o,"p"=>$p,"q"=>$q,"r"=>$r,"s"=>$s,"t"=>$t,"u"=>$u,"v"=>$v,"w"=>$w,
            "x"=>$x,"y"=>$y,"z" => $z];



        $numberOfLettersOfpossibleAnswer =  strlen($possibleAnswer);



        for ($ii = 0; $ii <= $numberOfLettersOfpossibleAnswer-1; $ii++)
        {

            $letterOfpossibleAnswer = substr($possibleAnswer, $ii, 1);


            switch ($letterOfpossibleAnswer) {
                case "A":
                    $a = $a+1;
                    $letterCounter["a"] = $a;
                    break;
                case "B":
                    $b = $b+1;
                    $letterCounter["b"] = $b;
                    break;
                case "C":
                    $c = $c+1;
                    $letterCounter["c"] = $c;
                    break;
                case "D":
                    $d = $d+1;
                    $letterCounter["d"] = $d;
                    break;
                case "E":
                    $e = $e+1;
                    $letterCounter["e"] = $e;
                    break;
                case "F":
                    $f = $f+1;
                    $letterCounter["f"] = $f;
                    break;
                case "G":
                    $g = $g+1;
                    $letterCounter["g"] = $g;
                    break;
                case "H":
                    $h = $h+1;
                    $letterCounter["h"] = $h;
                    break;
                case "I":
                    $i = $i+1;
                    $letterCounter["i"] = $i;
                    break;
                case "J":
                    $j = $j+1;
                    $letterCounter["j"] = $j;
                    break;
                case "K":
                    $k = $k+1;
                    $letterCounter["k"] = $k;
                    break;
                case "L":
                    $l = $l+1;
                    $letterCounter["l"] = $l;
                    break;
                case "M":
                    $m = $m+1;
                    $letterCounter["m"] = $m;
                    break;
                case "N":
                    $n = $n+1;
                    $letterCounter["n"] = $n;
                    break;
                case "O":
                    $o = $o+1;
                    $letterCounter["o"] = $o;
                    break;
                case "P":
                    $p = $p+1;
                    $letterCounter["p"] = $p;
                    break;
                case "Q":
                    $q = $q+1;
                    $letterCounter["q"] = $q;
                    break;
                case "R":
                    $r = $r+1;
                    $letterCounter["r"] = $r;
                    break;
                case "S":
                    $s = $s+1;
                    $letterCounter["s"] = $s;
                    break;
                case "T":
                    $t = $t+1;
                    $letterCounter["t"] = $t;
                    break;
                case "U":
                    $u = $u+1;
                    $letterCounter["u"] = $u;
                    break;
                case "V":
                    $v = $v+1;
                    $letterCounter["v"] = $v;
                    break;
                case "W":
                    $w = $w+1;
                    $letterCounter["w"] = $w;
                    break;
                case "X":
                    $x = $x+1;
                    $letterCounter["x"] = $x;
                    break;
                case "Y":
                    $y = $y+1;
                    $letterCounter["y"] = $y;
                    break;
                case "Z":
                    $z = $z+1;
                    $letterCounter["z"]= $z;
                    break;
            }
        }
        return $letterCounter;
    }



    public function checkAnswer($answer, $word)
    {
        $letterNumber = strlen($word);

        $repository = $this->get('scramble.repository.word');

        $allWords = $repository->findAllWords();

        foreach ($allWords as $wordObject) {
            $word = $wordObject->getWord();
            var_dump($word);
        }

    }

}

