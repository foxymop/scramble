<?php

namespace Andre\ScrambleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AndreScrambleBundle:Default:index.html.twig', array('name' => $name));
    }
}
