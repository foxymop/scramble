<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 06-07-2014
 * Time: 12:41
 */

namespace Andre\ScrambleBundle\Entity;


class Word extends \Andre\Scramble\Word {


    protected  $id;

    function __construct()
    {
        parent::__construct();
    }
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
} 