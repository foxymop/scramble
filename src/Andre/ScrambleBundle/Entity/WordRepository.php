<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 06-07-2014
 * Time: 13:11
 */

namespace Andre\ScrambleBundle\Entity;


use Andre\Scramble\WordRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class WordRepository extends EntityRepository implements WordRepositoryInterface
{

    public function findNumberOfWords()
    {
        $query = $this->createQueryBuilder('w')
            ->select('count(w)')
            ->getQuery();

        return $query->getSingleScalarResult();
    }
    public function findRandomWord($randomNumber)
    {
        $query = $this->createQueryBuilder('w')
            ->where('w.id = :randomNumber')
            ->setParameter('randomNumber', $randomNumber)
            ->getQuery();

        return $query->getResult();
    }

    public function findAllWords()
    {
        return $this->findAll();
    }

    public function insertWord($word)
    {
        $this->_em->persist($word);
        $this->_em->flush();
    }

} 