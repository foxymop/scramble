<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 06-07-2014
 * Time: 12:39
 */
namespace Andre\Scramble;

interface WordInterface
{
    /**
     * @param mixed $word
     */
    public function setWord($word);
    /**
     * @return mixed
     */
    public function getWord();
}