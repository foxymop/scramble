<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 19-07-2014
 * Time: 12:17
 */
namespace Andre\Scramble;

interface WordRepositoryInterface
{
    public function findNumberOfWords();

    public function findAllWords();

    public function findRandomWord($randomNumber);

    public function insertWord($word);
}