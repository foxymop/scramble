<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 06-07-2014
 * Time: 12:34
 */

namespace Andre\Scramble;


class Word implements WordInterface
{
    protected   $word;

    function __construct()
    {}

    /**
     * @param mixed $word
     */
    public function setWord($word)
    {
        $this->word = $word;
    }
    /**
     * @return mixed
     */
    public function getWord()
    {
        return $this->word;
    }
}